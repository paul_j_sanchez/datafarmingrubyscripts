#!/usr/bin/env ruby -w

# A class to efficiently perform Moving Average calculations.
# Calculating moving averages of length m on a set of data of length n
# requires Θ(m) storage and Θ(n) work.
#
# Author:: Paul J Sanchez (mailto:pjs at alum.mit.edu)
# Copyright:: Copyright (c) 2018 Paul J Sanchez
# License:: MIT
#
class MovingAverage
  # Number of elements in the moving average
  attr_reader :m

  # Initialize the MovingAverage object.
  #
  # *Arguments*::
  #   - +m+ -> the number of elements to be averaged
  #
  # *Raises*::
  #   - RuntimeError if +m+ < 1
  #
  def initialize(m)
    fail 'Number of terms to avg (m) must be strictly positive' if m < 1
    @m = m
    @current_set = Array.new(@m)
    @current_avg = 0.0
    @current_count = 0
  end

  # Add a new observation, get the resulting moving average.
  #
  # *Arguments*::
  #   - +x+ -> the number of elements to be averaged
  #
  # *Raises*::
  #   - RuntimeError if +x+ is non-numeric
  #
  # *Returns*::
  #   - Average of the last +m+ observations, or +nil+ if fewer than +m+ values have been processed.
  #
  def new_obs(x)
    x = x.to_f
    if @current_count < @m
      @current_set[@current_count] = x
      @current_count += 1
      @current_avg += (x - @current_avg) / @current_count
      @current_count == @m ? @current_avg : nil
    else
      @current_set << x
      @current_avg += (x - @current_set.shift) / @m
    end
  end
end

# Simple but effective test case
if __FILE__ == $PROGRAM_NAME
  puts "Ruby v" + RUBY_VERSION
  ma = MovingAverage.new(3)
  results = []
  20.times { |i| avg = ma.new_obs(i); results << avg if avg }
  puts results.join ', '
end
