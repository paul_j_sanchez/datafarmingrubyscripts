require 'fwt'

module RESV
  # Generate a Resolution V Fractional Factorial design for the specified
  # number of factors.  The design is guaranteed to yield unconfounded
  # interactions for all pairs of factors.  The design uses standardized
  # notation, i.e., -1 represents a low setting and 1 represents a high
  # setting for each factor.
  #
  # *Arguments*::
  #   - +number_of_factors+ -> the number of factors in your design. Limit is 120.
  # *Returns*::
  #   - a two-dimensional array specifying the design, where each column
  #     corresponds to a factor and each row is a design point.
  #
  # Author:: Paul J Sanchez (mailto:pjs@alum.mit.edu)
  # Copyright:: Copyright (c) 2023 Paul J Sanchez
  # License:: MIT
  #
  INDEX = [1, 2, 4, 8, 15, 16, 32, 51, 64, 85, 106, 128, 150, 171,
           219, 237, 247, 256, 279, 297, 455, 512, 537, 557, 594, 643, 803,
           863, 998, 1024, 1051, 1070, 1112, 1169, 1333, 1345, 1620, 1866,
           2048, 2076, 2085, 2185, 2372, 2456, 2618, 2800, 2873, 3127, 3284,
           3483, 3557, 3763, 4096, 4125, 4135, 4174, 4435, 4459, 4469, 4497,
           4752, 5255, 5732, 5804, 5915, 6100, 6369, 6907, 7069, 8192, 8263,
           8351, 8422, 8458, 8571, 8750, 8858, 9124, 9314, 9500, 10_026,
           10_455, 10_556, 11_778, 11_885, 11_984, 13_548, 14_007, 14_514,
           14_965, 15_125, 15_554, 16_384, 16_457, 16_517, 16_609, 16_771,
           16_853, 17_022, 17_453, 17_891, 18_073, 18_562, 18_980, 19_030,
           19_932, 20_075, 20_745, 21_544, 22_633, 23_200, 24_167, 25_700,
           26_360, 26_591, 26_776, 28_443, 28_905, 29_577, 32_705]
  POWER = [1, 2, 4, 8, 16, 16, 32, 64, 64, 128, 128, 128, 256, 256,
           256, 256, 256, 256, 512, 512, 512, 512, 1024, 1024, 1024, 1024,
           1024, 1024, 1024, 1024, 2048, 2048, 2048, 2048, 2048, 2048, 2048,
           2048, 2048, 4096, 4096, 4096, 4096, 4096, 4096, 4096, 4096, 4096,
           4096, 4096, 4096, 4096, 4096, 8192, 8192, 8192, 8192, 8192, 8192,
           8192, 8192, 8192, 8192, 8192, 8192, 8192, 8192, 8192, 8192, 8192,
           16_384, 16_384, 16_384, 16_384, 16_384, 16_384, 16_384, 16_384,
           16_384, 16_384, 16_384, 16_384, 16_384, 16_384, 16_384, 16_384,
           16_384, 16_384, 16_384, 16_384, 16_384, 16_384, 16_384, 32_768,
           32_768, 32_768, 32_768, 32_768, 32_768, 32_768, 32_768, 32_768,
           32_768, 32_768, 32_768, 32_768, 32_768, 32_768, 32_768, 32_768,
           32_768, 32_768, 32_768, 32_768, 32_768, 32_768, 32_768, 32_768,
           32_768, 32_768, 32_768]

  def RESV.make_design(number_of_factors)
    Array.new(number_of_factors) do |i|
      Array.new(POWER[number_of_factors], 0).tap { |a| a[INDEX[i]] = 1 }.hadamard
    end.transpose
  end

  def RESV.star_pts(k)
    Array.new(2*k+1) do |i|
      Array.new(k) { |j| (j+1) == (i+1) / 2 ? (i.odd? ? -1 : 1) : 0 }
    end
  end
end
