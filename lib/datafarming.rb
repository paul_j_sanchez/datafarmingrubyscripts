# frozen_string_literal: true

require_relative "datafarming/version"

module DataFarming
  class Error < StandardError; end
  # Your code goes here...
end
