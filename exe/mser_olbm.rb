#!/usr/bin/env ruby -w

require 'rubygems' if RUBY_VERSION =~ /^1\.8/
require 'colorize'

String.disable_colorization false

require 'optparse'
require 'datafarming/error_handling'
require 'datafarming/moving_average'

begin
  require 'quickstats'
rescue LoadError
  ErrorHandling.clean_abort [
    "\n\tALERT: quickstats gem is not installed!".red,
    "\tIf you have network connectivity, type:",
    "\n\t\tgem install quickstats\n".yellow,
    "\t(Admin privileges may be required.)\n\n"
  ]
end

help_msg = [
  'Calculate confidence intervals using overlapping batch means after MSER',
  'truncation.  Results are written to ' + 'stdout'.blue + ' in CSV format, with headers.',
  '', 'Syntax:',
  "\n\t#{ErrorHandling.prog_name} [--help] [--column COL] [filenames...]".yellow, '',
  "Arguments in square brackets are optional.  A vertical bar '|'",
  'indicates valid alternatives for invoking the option.  Prefix',
  'the command with "' + 'ruby'.yellow +
  '" if it is not on your PATH.', '',
  '  --help | -h | -? | ?'.green,
  "\tProduce this help message.",
  '  [--column COL | -c COL]'.green,
  "\tSpecify column to analyze. (Optional - default is column 1)",
  '  [filenames...]'.green,
  "\tThe names of one or more files containing data to be analyzed.",
  "\t(Optional - uses " + "stdin".blue + " if no files are specified.)"
]

OPTIONS = {:column => 0}

OptionParser.new do |opts|
  opts.banner = "Usage: #{$PROGRAM_NAME} [-h|--help] [filenames...]"
  opts.on('-h', '-?', '--help') { ErrorHandling.clean_abort help_msg }
  opts.on('-c COL',
          '--column COL',
          'Which column to average.',
          'Defaults to column 1.') { |col| OPTIONS[:column] = col.to_i - 1 }
end.parse!

ErrorHandling.clean_abort help_msg if ARGV[0] == '?'

def square(x)
  x * x
end

T_VALUE = [
  Float::INFINITY, 12.706, 4.303, 3.182, 2.776,
  2.571, 2.447, 2.365, 2.306, 2.262,
  2.228, 2.201, 2.179, 2.160, 2.145,
  2.131, 2.120, 2.110, 2.101, 2.093,
  2.086, 2.080, 2.074, 2.069, 2.064,
  2.060, 2.056, 2.052, 2.048, 2.045,
  2.042, 2.040, 2.037, 2.035, 2.032,
  2.030, 2.028, 2.026, 2.024, 2.023,
  2.021, 2.020, 2.018, 2.017, 2.015, 2.014
]

def olbm(data)
  data.shift if data[0] =~ /[A-Za-z]/ # strip header if one present
  data.map! { |line| line.strip.split(/[,:;]\s*|\s+/)[OPTIONS[:column]].to_f }
  mser = QuickStats.new
  warmup = [2 * data.length / 3, data.length - 10].min
  index = data.length - 1
  while index > (data.length - warmup) && index > 1
    mser.new_obs(data[index])
    index -= 1
  end
  best = [mser.std_err, mser.avg, index]

  while index > -1
    mser.new_obs(data[index])
    best = [mser.std_err, mser.avg, index] if mser.std_err <= best[0]
    index -= 1
  end

  avg = best[1]
  start_index = best[2]
  length = data.length - start_index
  m = [[length / 21, start_index].max, length / 3].min
  b = length / m
  m = length / b
  start_index += length - m * b
  ma = MovingAverage.new(m)

  sum_squared_deviations = data[start_index..-1].
    map { |y| ma.new_obs(y) }.compact.
    map { |y| square(y - avg)}.
    inject(&:+)

  se_sqr = (m.to_f / ((length - m) * (length - m + 1))) * sum_squared_deviations
  df = (3 * (b - 1) * (1 + (b - 1.0)**(-0.5 - 0.6 * b))).to_i / 2
  half_width = T_VALUE[df] * Math.sqrt(se_sqr)
  lower = avg - half_width
  upper = avg + half_width
  printf "%f,%f,%d,%f,%f\n", avg, Math.sqrt(se_sqr), df, lower, upper
end

puts "sample_mean,std_err,df,lower95_bound,upper95_bound"
if ARGF.filename == "-"
  data = STDIN.readlines
  olbm(data)
else
  ARGV.each do |fname|
    data = File.readlines(fname)
    olbm(data)
  end
end
