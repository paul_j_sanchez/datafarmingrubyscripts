#!/usr/bin/env ruby -w
# frozen_string_literal: true

# require 'colorize'
# String.disable_colorization false
# require 'datafarming/error_handling'

require 'optparse'
require 'yaml'

require 'datafarming/scale_design'

DESIGN_TYPES = %i[resv ccd rotatable nolh fbd fbs].freeze
REQUIRED_SPECS = %i[min max].freeze
ELECTIVE_SPECS = %i[id decimals].freeze
LEVELS = [17, 33, 65, 129, 257, 512].freeze
TWO_PI = 2.0 * Math::PI

HELP_MSG = [
  'Generate, scale, stack, and replicate a variety of statistical designs',
  "of experiments. You are required to specify the desired '--design TYPE'.",
  '',
  "Usage: #{File.basename($PROGRAM_NAME)} [options] [optional factor specs]"
].freeze

EXTENDED_HELP = [
  "====================\n",
  "Using the '-k' option will specify the number of factors for a standardized",
  'design, in which all factors are normalized to the range of [-1, 1].',
  '',
  'Optional factor specifications can be used to create a design which scales',
  'the factors to individualized ranges. Specifications are then required for',
  'all factors, and the number of factors will be derived from the number of',
  "specifications provided (thus overriding the '-k' option). Each factor's",
  'specification is separated from the others by whitespace.',
  '',
  'Factor specifications contain two to four comma-separated components, and',
  'cannot include any whitespace. The format is:',
  "\n   id:STRING,min:NUMBER,max:NUMBER,decimals:INTEGER\n",
  "Components can be arranged in any order.  The components 'min' and 'max'",
  'are required, and define the range of experimentation for this factor.',
  "Components 'id' and 'decimals' are optional. If 'id' is omitted, id",
  "values will be generated for use with the '--headers' option. If 'decimals'",
  'is omitted, the factors values will be displayed with default precision.',
  ' '
].freeze

EXAMPLES = [
  "\n====================\n",
  'EXAMPLE 1:',
  "\n  #{File.basename($PROGRAM_NAME)} --design fbd -k 3 --no-headers",
  "\n  Generate a standardized frequency-based design with three factors,",
  '  without headers. All ranges are -1..1 and output has default precision.',
  '',
  'EXAMPLE 2:',
  "\n  #{File.basename($PROGRAM_NAME)} -d nolh id:Fred,min:-10,max:10 decimals:5,max:100,min:0",
  "\n  Generate a NOLH for two factors. The first factor will have a column",
  "  header of 'Fred', and values will be scaled in the range -10..10. The",
  "  second column will have an automatically generated header 'X_002', and",
  '  values will be in the range 0..100 with five decimal places of precision.',
  "  As described in '--verbose-help', factor specifications must be separated",
  "  by whitespace.",
  '',
  'EXAMPLE 3:',
  "\n  #{File.basename($PROGRAM_NAME)} -d nolh -k 5 --output_yaml > my_template.yml",
  "\n  Generate a YAML template for a 5-factor NOLH design, and redirect it",
  "  to the file 'my_template.yml'. (The output file can be given any name",
  '  of your choosing.) The resulting YAML file has an intuitive human',
  "  readable format. It can be edited using your favorite programmer's",
  '  text editor (NOTE: word processors will not work!). After saving your',
  '  revisions, you can read it back in to generate the actual design using',
  "  the '--input_yaml' option:\n",
  "  #{File.basename($PROGRAM_NAME)} --input_yaml my_template.yml",
  ' '
].freeze

def prog_help(opts)
  puts opts
  exit
end

def verbose(opts)
  puts opts
  puts EXTENDED_HELP.join("\n")
  exit
end

def examples(opts)
  puts opts
  puts EXAMPLES.join("\n")
  exit
end

def fatal_err(msg)
  STDERR.puts msg
  exit 1
end

def resv(options)
  require 'datafarming/res_v_seqs'
  num_factors = options[:specs].size
  fatal_err("Too many factors for ResV designs") if num_factors > RESV::INDEX.size
  RESV.make_design num_factors
end

def ccd(options)
  require 'datafarming/res_v_seqs'
  num_factors = options[:specs].size
  fatal_err("Too many factors for CCD designs") if num_factors > RESV::INDEX.size
  RESV.make_design(num_factors) + RESV.star_pts(num_factors)
end

def rotatable(options)
  require 'datafarming/res_v_seqs'
  num_factors = options[:specs].size
  fatal_err("Too many factors for Rotatable designs") if num_factors > RESV::INDEX.size
  inv_len = 1.0 / Math.sqrt(num_factors)
  RESV.make_design(num_factors).each do |line|
    line.map! { |value| value * inv_len }
  end + RESV.star_pts(num_factors)
end

def nolh(options)
  require 'datafarming/nolh_designs'
  num_factors = options[:specs].size
  minimal_size =
    case num_factors
    when 1..7
      17
    when 8..11
      33
    when 12..16
      65
    when 17..22
      129
    when 23..29
      257
    when 30..100
      512
    else
      fatal_err "invalid number of factors: #{num_factors}"
    end
  options[:levels] ||= minimal_size
  fatal_err("Latin hypercube with #{options[:levels]} levels is " \
          "too small for #{num_factors} factors.") if options[:levels] < minimal_size
  NOLH::DESIGN_TABLE[options[:levels]]
end

def freqs2cols(options, design_set)
  num_factors = options[:specs].size
  fatal_err("No design (yet) for #{num_factors} factors") unless design_set.key? num_factors
  design_num = 0 # most have only one design after removing isomorphisms
  ds = design_set[num_factors]
  freqs = ds.freqs[design_num]
  Array.new(freqs.size) do |row|
    omega = freqs[row]
    v = Array.new(ds.nyq) do |i|
      f = ((i * omega) % ds.nyq).to_f / ds.nyq
      Math.sin(f * TWO_PI)
    end
    scale_factor = v.max
    v.map { |x| (x / scale_factor).round(15) }
  end.transpose
end

def fbd(options)
  require 'datafarming/freq_sets'
  freqs2cols(options, FBD::DESIGN_SETS)
end

def fbs(options)
  require 'datafarming/screen_freq_sets'
  freqs2cols(options, FBS::DESIGN_SETS)
end

def validate_opts(options)
  required_options = [:design]
  missing = required_options - options.keys
  fatal_err("Missing required options: --#{missing.join(', --')}") unless missing.empty?
  options[:design] = options[:design].to_sym
end

def validate(spec)
  unknown = spec.keys - REQUIRED_SPECS - ELECTIVE_SPECS
  fatal_err("Unknown factor spec(s): #{unknown}") unless unknown.empty?
  missing = REQUIRED_SPECS - spec.keys
  fatal_err("Factor spec #{spec} missing #{missing}") unless missing.empty?
end

def optional_specs(factor_spec, i)
  YAML.safe_load("{#{factor_spec}}".gsub(':', ': '),
    permitted_classes: [Symbol]).tap do |spec|
    spec.transform_keys!(&:to_sym)
    spec[:decimals] ||= nil
    spec[:id] ||= "X_#{format('%03d', (i + 1))}"
    validate(spec)
  end
end

def parse_specs(options, remainder)
  if remainder.size.positive?
    remainder.each.with_index do |factor_spec, i|
      options[:specs] << optional_specs(factor_spec, i)
    end
  end
end

def parse_args
  default_options = {
    headers: true,
    replicate: 1,
    stack: 1,
    center: false,
    specs: []
  }

  yaml_input = false
  if ARGV.size > 2 && ARGV.any? { |elt| elt.eql?("-i") || elt.eql?("--input_yaml") }
    STDERR.puts 'NOTE: All other arguments ignored when using YAML input'
  end

  options = default_options

  begin
    remainder = OptionParser.new do |opts|
      opts.banner = HELP_MSG.join("\n")
      opts.on('-h', '--help', 'Prints help') { prog_help(opts) }
      opts.on('-v', '--verbose-help', 'Print more verbose help') { verbose(opts) }
      opts.on('-x', '--examples', 'Show some examples') { examples(opts) }
      opts.on('-d', '--design TYPE', DESIGN_TYPES,
              'REQUIRED: Type of design, one of:',
              "  #{DESIGN_TYPES.join ' '}")
      opts.on('-k', '--num_factors QTY', /[1-9][0-9]*/, Integer,
              'Number of factors',
              'Ignored if optional factor specs provided') do |k|
        options[:specs] = Array.new(k) do |i|
          {id: "X_#{format('%03d', (i + 1))}", :min => -1, :max => 1, :decimals => nil}
        end
        k
      end
      opts.on('-r', '--replicate QTY', /[1-9][0-9]*/, Integer,
              'Replicate QTY times', '(default: 1)')
      opts.on('-s', '--stack QTY', /[1-9][0-9]*/, Integer,
              'Stack QTY times', '(default: 1)')
      opts.on('--[no-]center', 'NOTE: Applies only to stacked designs',
              'Stacking includes center pt','(default: no-center)')
      opts.on('--[no-]headers', 'Output has column headers', '(default: headers)')
      opts.on('-l', '--levels QTY', Regexp.new("(#{LEVELS.join(')|(')})"),
              Integer, 'NOTE: Applies only to NOLH designs',
              'Number of levels in the base NOLH:',
              "  #{LEVELS.join ' '}")
      opts.on('-o', '--output_yaml', 'Write design YAML to STDOUT')
      opts.on("-i PATH", "--input_yaml", 'Read design YAML from PATH',
              'Mutually exclusive with all other options, prints',
              'warning to STDERR if other options are passed') do |path|
        ARGV.clear
        options = {specs: []}
        yaml_input = true
        YAML::safe_load_file(path, permitted_classes: [Symbol]).each do |k, v|
          options.merge!({k.to_sym => v})
        end
      end
    end.parse!(into: options)
  rescue OptionParser::ParseError => error
    fatal_err("#{error}\n(-h or --help will show valid options)")
  end

  fatal_err("Levels option only valid for NOLH designs") if options[:levels] && options[:design] != :nolh

  remainder = [] if yaml_input
  if remainder.size.positive? && options[:specs].size.positive?
    STDERR.puts 'NOTE: Num_factors ignored when using optional factor specs'
    options[:specs] = []
  end
  options.delete(:num_factors) if options.include?(:num_factors)

  parse_specs(options, remainder)
  validate_opts options
  options
end

def stack(design, num_stacks, centered)
  return design if num_stacks < 2
  fatal_err("Stacking #{num_stacks} times exceeds number of design columns") if num_stacks > design[0].length
  num_stacks -= 1
  result = design.transpose
  if num_stacks > 0
    design_t = if centered
      design.transpose
    else
      design.delete_if {|row| row.all? &:zero? }.transpose
    end
    num_stacks.times do
      design_t.rotate!
      result.map!.with_index { |line, i| line + design_t[i] }
    end
  end
  result.transpose
end

ARGV << '--help' if ARGV.empty?

options = parse_args

if options[:output_yaml]
  options.delete :output_yaml
  puts options.to_yaml
else
  base_design = method(options[:design]).call(options)
  separator = ','
  puts Array.new(options[:specs].size) { |i| options[:specs][i][:id] }.join(separator) if options[:headers]

  # stack
  design = stack(base_design, options[:stack], options[:center])
           .transpose.first(options[:specs].size).transpose

  # scale
  scaler = options[:specs].map do |spec|
    Scaler.new(min: spec[:min], max: spec[:max], decimals: spec[:decimals])
  end

  # replicate
  options[:replicate].times do
    design.each do |line|
      puts line.map.with_index { |v, i| scaler[i].scale(v) }.join(separator)
    end
  end
end
