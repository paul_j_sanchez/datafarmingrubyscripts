#!/usr/bin/env ruby -w

# Efficiently calculate a moving average

require 'colorize'

String.disable_colorization false

require 'optparse'
require 'datafarming/error_handling'
require 'datafarming/moving_average'

help_msg = [
  'Calculate moving averages for one or more input files, or for ' +
    'stdin'.green + '.', 'Results are sent to ' + 'stdout'.green + '.', '',
  'Syntax:' + "\t#{ErrorHandling.prog_name} [OPTIONS] [FILENAMES...]".yellow, '',
  "Prefix the command with '" + 'ruby'.yellow +
    "' if it is not on your PATH.", '',
  'Options:', ''
]

opts_help = nil
options = { column: 0 }
OptionParser.new do |opts|
  opts.banner = help_msg.join("\n")
  opts.on('-h', '-?', '--help', 'Print this help') do
    puts
    puts opts
    puts
    exit
  end
  opts.on('-w LEN',
          '--window LEN',
          'Window size to use for moving average',
          'average ' + '[REQUIRED]'.red) { |win| options[:window] = win.to_i }
  opts.on('-c COL',
          '--column COL',
          'Which column to average.',
          'Defaults to column 1.') { |col| options[:column] = col.to_i - 1 }
  opts_help = opts
end.parse!

ErrorHandling.clean_abort [opts_help] if ARGV[0] == '?' || options[:window].nil?

column = options[:column]
ma = MovingAverage.new(options[:window])
ARGF.each do |line|
  value = line.strip.split(/[,:;]\s*|\s+/).map(&:to_f)
  result = ma.new_obs(value[column]) unless value[column].nil?
  puts result unless result.nil?
end
