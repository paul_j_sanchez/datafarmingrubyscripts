# -*- ruby -*-

require 'date'
require_relative "lib/datafarming/version"

Gem::Specification.new do |s|
  s.name = "datafarming"
  s.version = DataFarming::VERSION
  s.date = "#{Date.today}"
  s.summary = "Useful scripts for data farming."
  s.homepage = "https://bitbucket.org/paul_j_sanchez/datafarmingrubyscripts"
  s.email = "pjs@alum.mit.edu"
  s.description = "Ruby scripts for data farming, including pre- and" \
    "post-processing, design generation and scaling, and run control."
  s.author = "Paul J Sanchez"
  s.files = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test/|features/|.gitignore)})
  end
  s.bindir = "exe"
  s.executables = s.files.grep(%r{^exe/}) { |f| File.basename(f) }
  s.require_paths = ["lib"]
  s.add_runtime_dependency 'fwt', '~> 1.2'
  s.add_runtime_dependency 'colorize', '~> 1'
  s.add_runtime_dependency 'quickstats', '~> 2'
  s.add_runtime_dependency 'optparse', '~> 0.4'
  s.add_runtime_dependency 'yaml', '~> 0.3'
  s.required_ruby_version = '>= 2.6'
  s.license = 'MIT'
end
